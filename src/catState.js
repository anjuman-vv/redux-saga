
import { createSlice } from '@reduxjs/toolkit'
import React from 'react'

export const catSlice = createSlice({
    name : 'cats',
    initialState :{
        cats:[],
        isLoading : false
    },

    reducers : {
        getCatsFetch : (state) => {
            state.isLoading = true;
        },

        getCatsSuccess : ( state, actions) => {
            state.cats = actions.payload;
            state.isLoading = false;
        },
        getCatsFailure : ( state ) => {
           
            state.isLoading = false;
        }
    }

});

export const { getCatsFetch, getCatsSuccess, getCatsFailure } = catSlice.actions;

export default catSlice.reducer;
