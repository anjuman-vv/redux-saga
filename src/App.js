
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import { getCatsFetch } from './catState';

function App() {
 const cats = useSelector(state => state.cats.cats);

 const dispatch = useDispatch();

 useEffect(() => {
  dispatch(getCatsFetch());
 },[dispatch])

 console.log(cats);
  return (
    <div className="App">
     <h1>Cat Species Gallery</h1>
     <p>images of d fgh trb ths anbi at r sdfgh gfd fub fdfgh cvbm hgfd</p>
      <hr></hr>
    <div className='Gallery'>
      {cats.map(c =>
        <div key={c.id} className='row'>
          <div className='column column-left'>
            <img alt={c.name}
            src={c.image.url}
            width="200"
            height="200"
            />
          </div>
          <div className='column column-right'>
            <h2>{c.name}</h2>
            <h5>Temperament:{c.temperament}</h5>
            <p>{c.description}</p>
          </div>
        </div>
        )}
    </div>
    </div>
  );
}

export default App;

// npm i redux-saga react-router-dom redux